<!DOCTYPE html>
<html>

<head>
	<title>CrudTest</title>
</head>

<body>
	<div class="row">
		<div class="col-sm-4">
			<h3>Rak Buku</h3>
		</div>
		<div class="col-sm-4">
			<a href="/bookshelf/add"> + Tambahkan Buku</a>
		</div>
	</div>
	<br />
	<br />
	<div class="col-12">
		<div>
			<table border="1">
				<thead>
					<tr>
						<th>Id</th>
						<th>Judul Buku</th>
						<th>Genre</th>
						<th>Penerbit</th>
						<th>Penulis</th>
						<th>Kategori</th>
					</tr>
				</thead>
				<div class="container">
					@foreach($rak_buku as $rb)
					<tbody>
						<tr>
							<td scope="row"> {{ $rb->buku_id }} </td>
							<td>{{ $rb->buku_judul }}</td>
							<td>{{ $rb->buku_genre }}</td>
							<td>{{ $rb->buku_penerbit }}</td>
							<td>{{ $rb->buku_penulis }}</td>
							<td>{{ $rb->buku_kategori }}</td>
							<td><a href="/bookshelf/edit/{{ $rb->buku_id }}">Edit</a></td>
							<td><a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="/bookshelf/hapus/{{ $rb->buku_id }}"><i class="fa fa-trash">Delete</i></a></td>
						</tr>
					</tbody>
				</div>
				@endforeach
			</table>

			<style type="text/css">
				.pagination li {
					float: left;
					list-style-type: none;
					margin: 5px;
				}
			</style>
			<div id="pagination">
				{{ $rak_buku->links() }}
			</div>
		</div>
	</div>
	<br />

</body>

</html>