<!DOCTYPE html>
<html>

<head>
	<title>Add Data</title>
</head>

<body>
	<h3>Tambah Daftar Buku</h3>

	<a href="/bookshelf"> Go Back</a>

	<br />
	<br />

	<form action="/bookshelf/input" method="post">
		{{ csrf_field() }}
		Judul <input type="text" name="buku_judul" required="required"> <br /><br>
		Genre <input type="text" name="buku_genre" required="required"> <br /><br>
		Penerbit<input type="text" name="buku_penerbit" required="required"> <br /><br>
		Penulis<input type="text" name="buku_penulis" required="required"> <br /><br>
		<select name="buku_kategori" required="required" class="form-control">
		<option value="">Kategori</option>
			<option value="{{ 'Matematika' }}">Matematika</option>
			<option value="{{ 'Biologi' }}">Biologi</option>
            <option value="{{ 'Fisika' }}">Fisika</option>
            <option value="{{ 'Kimia' }}">Kimia</option>
            <option value="{{ 'Lainnya' }}">Lainnya</option>
        </select>
		<input type="submit" value="Input">
	</form>

</body>

</html>