<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//CRUD

Route::get('/bookshelf','BookshelfController@index');

Route::get('/bookshelf/add','BookshelfController@addData');

Route::post('/bookshelf/input','CrudController@input');

Route::get('/bookshelf/edit/{buku_id}','BookshelfController@editData');

Route::post('/bookshelf/update','CrudController@update');

Route::get('/bookshelf/hapus/{buku_id}','CrudController@delete');

Route::get('/article', 'WebController@index');