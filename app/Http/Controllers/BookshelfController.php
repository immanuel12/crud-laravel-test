<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookshelfController extends Controller
{
	public function index()
	{
		$rak_buku = DB::table('rak_buku')->paginate(5);
		return view('index', ['rak_buku' => $rak_buku]);
	}

	public function addData()
	{
		return view('addData');
	}
    
    public function editData($id)
	{
		$rak_buku = DB::table('rak_buku')->where('buku_id', $id)->get();
		return view('editData', ['rak_buku' => $rak_buku]);
	}
    
}